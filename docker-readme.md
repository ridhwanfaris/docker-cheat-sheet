# DOCKER COMMAND

## How to build image

```
docker build -t image_name .
```

## How to check image list

```
docker image ls
```

## How to run image

```
docker run -p 8090:9090 image_name
```

```
note: port 8090 adalah port yang di define di dockerfile. sedangkan port 9090 adalah port dari appsnya (eg. server.port=9090)
```

## How to start container

```
docker start container_id
```

## How to stop image running

```
docker stop container_id
```

## How to delete image

```
docker rmi image_id
```

## How to delete image forcely

```
docker rmi -f image_id
```

## How to check container running

```
docker ps
```

## How to view all containers

```
docker ps -a
```

## Stop All Containers

```
docker stop $(docker ps -a -q)
```

## Remove All Containers

```
docker rm $(docker ps -a -q)
```

## Show all containers that running

```
docker ps --format {{.ID}}
```

## Stop All Containers that running

```
docker stop $(docker ps --format {{.ID}})
```

## Push image to docker hub

```
docker tag [image name] [repository's name]:[version]
```

```
docker push [repository's name]:[version]
```

## Pull image from docker hub

```
docker login --username cokiMuslim --password demo212
```

```
docker pull [repository's name]:[version]
```

## Remove All Image
```
docker rmi -f $(docker images --format {{.ID}})
```